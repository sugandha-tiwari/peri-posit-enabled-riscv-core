/*
Author: Sugandha Tiwari
Description: Fraction div sqrt using Hardfloat
*/

module frac_div_sqrt#(parameter sigWidth = 3) (
        nReset,
        clock,
        inReady,
        inValid,
        sqrtOp,
        normalCase_S,
        oddSqrt_S,
        evenSqrt_S,
        sigA_S,
        sigB_S,
        outValid,
        out_sig
    );
    
    input nReset;
    input clock;
    output inReady;
    input inValid;
    input sqrtOp;
    input normalCase_S;
    input oddSqrt_S;
    input evenSqrt_S;
    input [sigWidth:0] sigA_S;
    input [sigWidth:0] sigB_S;
    output outValid;
    output [(sigWidth + 2):0] out_sig;
    
    function integer clog2;
      input integer a;
      begin
          a = a - 1;
          for (clog2 = 0; a > 0; clog2 = clog2 + 1) a = a>>1;
      end
    endfunction

    reg [(clog2(sigWidth + 3) - 1):0] cycleNum;
    reg sqrtOp_Z;
    reg [(sigWidth - 2):0] fractB_Z;
    reg [(sigWidth + 1):0] rem_Z;
    reg notZeroRem_Z;
    reg [(sigWidth + 1):0] sigX_Z;
    wire idle = (cycleNum == 0);
    assign inReady = (cycleNum <= 1);
    wire entering = inReady && inValid;
    wire entering_normalCase = entering && normalCase_S;
    wire skipCycle2 = (cycleNum == 3) && sigX_Z[sigWidth + 1];
    
    always @(negedge nReset, posedge clock) begin
      //if(cycleNum==(sigWidth + 2)) $display($time, "\tsigA_S:%h sigB_S:%h",sigA_S,sigB_S);
      //$display($time, "\tcycleNum: %0d inValid:%b", cycleNum,  inValid);
      if (!nReset) begin
        cycleNum <= 0;
      end else begin
        if (!idle || inValid) begin
          cycleNum <=
              (entering && !normalCase_S ? 1 : 0)
            | (entering_normalCase
                   ? (sqrtOp ? (oddSqrt_S ? sigWidth : sigWidth + 1)
                          : sigWidth + 2)
                   : 0)
            | (!idle && !skipCycle2 ? cycleNum - 1 : 0)
            | (!idle &&  skipCycle2 ? 1            : 0);
        end
      end
    end
    
    always @(posedge clock) begin
      if (entering) begin
          sqrtOp_Z   <= sqrtOp;
      end
      if (entering_normalCase && !sqrtOp) begin
          fractB_Z <= sigB_S[(sigWidth - 2):0];
      end
    end
    
    wire [1:0] decHiSigA_S = sigA_S[(sigWidth - 1):(sigWidth - 2)] - 1;
    wire [(sigWidth + 2):0] rem =
        (inReady && !oddSqrt_S ? sigA_S<<1 : 0)
      | (inReady &&  oddSqrt_S
             ? {decHiSigA_S, sigA_S[(sigWidth - 3):0], 3'b0} : 0)
      | (!inReady ? rem_Z<<1 : 0);
    wire [sigWidth:0] bitMask = ({{(sigWidth + 2){1'b0}}, 1'b1}<<cycleNum)>>2;
    wire [(sigWidth + 1):0] trialTerm =
        ( inReady && !sqrtOp    ? sigB_S<<1           : 0)
      | ( inReady && evenSqrt_S ? 1<<sigWidth         : 0)
      | ( inReady && oddSqrt_S  ? 5<<(sigWidth - 1)   : 0)
      | (!inReady && !sqrtOp_Z  ? {1'b1, fractB_Z}<<1 : 0)
      | (!inReady &&  sqrtOp_Z ? sigX_Z<<1 | bitMask : 0);
    wire signed [(sigWidth + 3):0] trialRem = rem - trialTerm;
    wire newBit = (0 <= trialRem);
    
    always @(posedge clock) begin
      if (entering_normalCase || (cycleNum > 2)) begin
          rem_Z <= newBit ? trialRem : rem;
      end
      if (entering_normalCase || (!inReady && newBit)) begin
          notZeroRem_Z <= (trialRem != 0);
          sigX_Z <=
                ( inReady && !sqrtOp   ? newBit<<(sigWidth + 1) : 0)
              | ( inReady &&  sqrtOp   ? 1<<sigWidth            : 0)
              | ( inReady && oddSqrt_S ? newBit<<(sigWidth - 1) : 0)
              | (!inReady              ? sigX_Z | bitMask       : 0);
      end
//      $display($time, "\tcyclenum:%d sigX_Z: %h inReady:%b sqrtOp:%b newBit:%b", cycleNum,  sigX_Z,  inReady,  sqrtOp,  newBit);
    end
    
    assign outValid = (cycleNum == 1);
    assign out_sig  = {sigX_Z, notZeroRem_Z};

endmodule

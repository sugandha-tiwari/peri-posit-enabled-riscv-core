/*
Author: Sugandha Tiwari
Description: Posit encoding
*/
package enc;
  
  `include "defined_params.bsv"

  (*noinline*)
  function Bit#(`pwidth) fn_enc(Tuple5#(Bit#(2), Bit#(1), Int#(`exp1), Bit#(`frac1), Bit#(1)) op1);
    
    let p = valueOf(`pwidth);
    let {flag, sign, expo, final_frac, sticky_bit} = op1;
  	
  	Bit#(`es) es_bit = pack(truncate(expo));
  	Int#(`exp1mes) k = unpack(truncateLSB(pack(expo)));
  	Bit#(`esfrac) frac_es = {es_bit,final_frac};
		
  	Bit#(`exp1mesm1) count = truncate(pack(abs(k)));  	
  	
  	Bit#(`pwidth) regime = 1;
  	if(pack(k)[valueOf(`exp1mes)-1] == 0)	begin
  		regime = '1;
  		regime[0] = 0;
  		count = count + 1;
  	end  	
  	
  	Bit#(`epwidth) ext_result = {regime,frac_es};
  	
  	Int#(`exp1mesm1) regime_shift = fromInteger(valueOf(`pm1)) - unpack(count);
  	if(pack(regime_shift)[valueOf(`exp1mesm1)-1] == 0) begin
  		ext_result = ext_result << regime_shift;
  	end
  	ext_result = ext_result >> 1;
  	
  	Bit#(`pwidth) result = truncateLSB(ext_result);
  	Bit#(`esfrac) sticky_part = truncate(ext_result);  
  	
  	Bit#(1) guard_bit = result[0];
  	Bit#(1) round_bit = sticky_part[fromInteger(valueof(`esfrac))-1];
  	sticky_part = sticky_part << 1;
  	sticky_bit = sticky_bit | (|sticky_part);

  	bit round = round_bit & (guard_bit | sticky_bit);
  	
  	Bit#(`pm1) rtemp = result[valueOf(`pm1)-1:0];
  	Bit#(1) max_pos = (~result[p-1])&(&rtemp);
  	if(max_pos == 1) round = 0;
  	bit plus_one = 0;
  	plus_one = round ^ sign;		
  	if((round|(|result)) == 0) result = 1;
  	if(sign==1) result = ~result;
  	result = result + zeroExtend(plus_one);
  	if(flag[0] == 1) result =0;
  	if(flag[1] == 1) begin result = 0; result[valueOf(`pm1)] = 1; end
  	return result;
		
  endfunction
  
endpackage

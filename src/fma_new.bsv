/*
Author: Sugandha Tiwari
Description: Posit FMA
*/
package fma_new;

  import unsignedmul :: * ;
  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;  
  import dec :: *;
  import enc :: *;
  import bsvfma :: *;
  
  `include "defined_params.bsv"

  interface Ifc_fma;
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, bit operation, bit neg, Bit#(`es) esval);
    method Bit#(`pwidth) resp;
    method Bool resp_valid;
  endinterface
  
  module mkfma(Ifc_fma);
    
    Vector#(`fma_stages, Reg#(Bit#(`pwidth)))  rg_inputs  <- replicateM(mkReg(unpack(0)));
    Vector#(`fma_stages, Reg#(Bool)) rg_valid  <- replicateM(mkDReg(False));
    //Ifc_unsignedmul#(`frac1, `frac1) mult <- mkunsignedmul;
    Ifc_bsvfma fma <- mkbsvfma();
    
    rule rl_start;
      for(Integer i = 1; i <=  `fma_stages-1 ; i = i+ 1) begin
        rg_inputs[i] <= rg_inputs[i - 1];
        rg_valid[i] <= rg_valid[i-1];
      end
      
    endrule
    
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, bit operation, bit neg, Bit#(`es) esval);        
        
        fma.req(op1,  op2,  op3, neg, operation);
        rg_inputs[0] <=  fma.resp;
        rg_valid[0] <=  True;
    endmethod
    
    method Bit#(`pwidth) resp;
      return rg_inputs[`fma_stages -1];
    endmethod
    
    method Bool resp_valid;
      return rg_valid[`fma_stages-1];
    endmethod
    
  endmodule
  
endpackage

/*
Author: Sugandha Tiwari
Description: Posit FMA
*/
package fma;

  import unsignedmul :: * ;
  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;  
  import dec :: *;
  import enc :: *;
  
  `include "defined_params.bsv"

  interface Ifc_fma;
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, bit operation, bit neg, Bit#(`es) esval);
    method Bit#(`pwidth) resp;
    method Bool resp_valid;
  endinterface
  
  module mkfma(Ifc_fma);
    
    Vector#(`fma_stages, Reg#(Bit#(`pwidth)))  rg_inputs  <- replicateM(mkReg(unpack(0)));
    Vector#(`fma_stages, Reg#(Bool)) rg_valid  <- replicateM(mkDReg(False));
    Ifc_unsignedmul#(`frac1, `frac1) mult <- mkunsignedmul;
    
    rule rl_start;
      for(Integer i = 1; i <=  `fma_stages-1 ; i = i+ 1) begin
        rg_inputs[i] <= rg_inputs[i - 1];
        rg_valid[i] <= rg_valid[i-1];
      end
      
    endrule
    
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, bit operation, bit neg, Bit#(`es) esval);        
        
        let {s1, e1, f1, flags1} = fn_dec(op1, esval);
        let {s2, e2, f2, flags2} = fn_dec(op2, esval);
        let {s3, e3, f3, flags3} = fn_dec(op3, esval);
        
        Bit#(1) grt_sign = s1^s2^neg;
        Int#(`exp1) grt_exp =  signExtend(e1) + signExtend(e2);
        mult.ia({1'b1, f1});
        mult.ib({1'b1, f2});
        Bit#(`frac2) grt_frac = mult.oc;
        //$display("1 %b %d", grt_frac,  grt_exp);

        if(grt_frac[valueof(`frac2)-1] == 1)
          grt_exp = grt_exp + 1;
        else
          grt_frac = grt_frac << 1;
        //$display("2 %b %d", grt_frac,  grt_exp);
        
        Bit#(1) sml_sign = s3^operation^neg;
        Int#(`exp1) sml_exp =  signExtend(e3);
        Bit#(`frac1) zeros = 0;
        Bit#(`frac2) sml_frac = {1'b1,f3,zeros};
        
        Bit#(1) res_sign = 0;
        Int#(`exp1) res_exp = 0;
        Bit#(`frac3) res_frac = 0;
        Bit#(2) resultFlag = 0;
        Bit#(1) sticky_bit = 0;
        
        Bool swap = False;
        
        Int#(`exp2) exp_diff = signExtend(grt_exp) - signExtend(sml_exp);
        //$display("%d %d", grt_exp,  sml_exp);
        Bit#(1) or_exp_diff = |pack(exp_diff);

        if(pack(exp_diff)[valueof(`exp2)-1]==1) begin
          swap = True;
        end
        else if(or_exp_diff == 0 && sml_frac > grt_frac) begin
          swap = True;
        end
        else if(or_exp_diff == 0 && sml_frac == grt_frac && sml_sign != grt_sign) begin
          resultFlag[0] = 1;
        end
        
        Bool rset = False;
        bit or_1_2 = flags1[0] | flags2[0];
        
        if((flags1[1] | flags2[1] | flags3[1]) ==1) begin
        	resultFlag[1] = 1;
        end
        
        else if((or_1_2 & flags3[0]) ==1) begin
          resultFlag[0] = 1;
        end
        
      	else if(or_1_2 ==1) begin
        	res_sign = sml_sign;
        	res_exp = sml_exp;
        	res_frac = {1'b0, sml_frac};
        	rset = True;
        end
        
        else if(flags3[0] == 1) begin
          res_sign = grt_sign;
        	res_exp = grt_exp;
        	res_frac = {1'b0, grt_frac};
        	rset = True;
		    end

        if(swap) begin
          Bit#(1) temp_sign = grt_sign;
          Int#(`exp1) temp_exp = grt_exp;
          Bit#(`frac2) temp_frac = grt_frac;
          grt_sign = sml_sign; grt_exp = sml_exp; grt_frac = sml_frac;
          sml_sign = temp_sign; sml_exp = temp_exp; sml_frac = temp_frac;
          exp_diff = abs(exp_diff);
        end     

        Int#(`exp2) sticky_shift = fromInteger(valueof(`frac2)) - exp_diff;
        Bit#(`frac2) sticky_part = sml_frac;
        
        if(pack(sticky_shift)[valueof(`exp2)-1]==0) begin
          sticky_part = sml_frac << sticky_shift;
        end
        sticky_bit = |sticky_part;

        sml_frac = sml_frac >> exp_diff;

        Bit#(`frac3) fma_frac = 0;
        if(grt_sign == sml_sign)
          fma_frac = zeroExtend(grt_frac) + zeroExtend(sml_frac);
        else
          fma_frac = zeroExtend(grt_frac) - zeroExtend(sml_frac);

        $display("3 %d %b",  grt_exp, fma_frac);

        if(fma_frac[valueof(`frac3)-1]==1) begin
          sticky_bit = sticky_bit | fma_frac[0];
          fma_frac = fma_frac >> 1;
          grt_exp = grt_exp + 1;
        end

        else if(fma_frac[valueof(`frac3)-2]!=1) begin
          let shift = countZerosMSB(fma_frac);
          shift = shift - 1;
          fma_frac = fma_frac << shift ;
          grt_exp = grt_exp - unpack(zeroExtend(pack(shift)));
        end
        //$display("4 %d %b",  grt_exp, fma_frac);
        
        if(rset) begin
          grt_sign = res_sign;
          grt_exp = res_exp;
          fma_frac = res_frac;
          sticky_bit = 0;
        end    
		        
        Bit#(TSub#(`frac3,2)) trunc_fma_frac = truncate(fma_frac);
		    Bit#(`frac) sticky_bits = truncate(trunc_fma_frac);
		    sticky_bit = sticky_bit |(|sticky_bits);
		    Bit#(`frac1) new_frac = truncateLSB(trunc_fma_frac);
        //$display("%b %b %d %b %b",resultFlag, grt_sign, grt_exp, new_frac, sticky_bit);
        //rg_inputs[0] <=  fn_enc(tuple5(resultFlag, grt_sign, grt_exp, new_frac, sticky_bit), esval);

        //////////
        //let {flag, sign, expo, final_frac, sticky_bit} = op1;
        let flag = resultFlag;
        let sign = grt_sign;
        let expo = grt_exp;
        let final_frac = new_frac;
        //let esval = 2;

        Bit#(`es) es_bit = pack(truncate(expo));
      Int#(`exp1mes) k = unpack(truncateLSB(pack(expo)));
      Bit#(`esfrac) frac_es = {es_bit,final_frac};
      Bit#(`exp1mesm1) count = truncate(pack(abs(k)));

         	  Bit#(`pwidth) regime = 1;
      if(pack(k)[valueOf(`exp1mes)-1] == 0) begin
        regime = '1;
        //$display("1 %b", regime);
        regime[0] = 0;
        //$display("2 %b", regime);
        count = count + 1;
      end

      Bit#(`epwidth) ext_result = {regime,frac_es};
      //$display("3 %b", ext_result);

      Int#(`exp1mesm1) regime_shift = fromInteger(valueOf(`pm1)) - unpack(count);
      if(pack(regime_shift)[valueOf(`exp1mesm1)-1] == 0) begin
        ext_result = ext_result << regime_shift;
      end
      //$display("4 %b", ext_result);
      ext_result = ext_result >> 1;
      //$display("5 %b", ext_result);

      Bit#(`pwidth) result = truncateLSB(ext_result);
      $display("6 %b", result);
      Bit#(`esfrac) ssticky_part = truncate(ext_result);

      Bit#(1) guard_bit = result[0];
     // $display("7 %b",guard_bit);
      Bit#(1) round_bit = ssticky_part[fromInteger(valueof(`esfrac))-1];
      $display("7 %b", round_bit);
      ssticky_part = ssticky_part << 1;
      sticky_bit = sticky_bit | (|ssticky_part);
      $display("8 %b", sticky_bit);

      bit round = round_bit & (guard_bit | sticky_bit);
      $display("9 %b",round_bit);

      Bit#(`pm1) rtemp = result[valueOf(`pm1)-1:0];
      Bit#(1) max_pos = (~result[`pwidth-1])&(&rtemp);
      if(max_pos == 1) round = 0;
      bit plus_one = 0;
      plus_one = round ^ sign;
      if((round|(|result)) == 0) result = 1;
      if(sign==1) result = ~result;
      $display("10 %b", result);
      result = result + zeroExtend(plus_one);
      $display("11 %b", result);
      if(flag[0] == 1) result =0;
      if(flag[1] == 1) begin result = 0; result[valueOf(`pm1)] = 1; end
   //return result;
   rg_inputs[0] <=  result;

   //////
		    
        //rg_inputs[0] <=  fn_enc(tuple5(resultFlag, grt_sign, grt_exp, new_frac, sticky_bit), esval);
        rg_valid[0] <=  True;
    endmethod
    
    method Bit#(`pwidth) resp;
      return rg_inputs[`fma_stages -1];
    endmethod
    
    method Bool resp_valid;
      return rg_valid[`fma_stages-1];
    endmethod
    
  endmodule
  
endpackage

/*
Author: Sugandha Tiwari
Description: Posit ptoi
*/
package ptoi;

  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;
  import BUtils :: *;
  import dec :: *;
  `include "defined_params.bsv"

  interface Ifc_ptoi;
    method Action req(Bit#(`pwidth) op1, bit unsign, Bit#(3) rm);
    method Bit#(`pwidth) resp;
    method Bool resp_valid;
  endinterface
  
  module mkptoi(Ifc_ptoi);
    
    Vector#(`ptoi_stages, Reg#(Bit#(`pwidth)))  rg_inputs  <- replicateM(mkReg(unpack(0)));
    Vector#(`ptoi_stages, Reg#(Bool)) rg_valid  <- replicateM(mkDReg(False));
    
    rule rl_start;
      for(Integer i = 1; i <=  `ptoi_stages-1 ; i = i+ 1) begin
        rg_inputs[i] <= rg_inputs[i - 1];
        rg_valid[i] <= rg_valid[i-1];
      end
      
    endrule
    
    method Action req(Bit#(`pwidth) op1, bit unsign, Bit#(3) rm);
      
      let {s1, expo, f1, flags} = fn_dec(op1);      
      let p = fromInteger(valueOf(`pwidth));
      let pf = fromInteger(valueOf(`pfrac));
      let fr = fromInteger(valueOf(`frac));
      
      Bit#(`pwidth) result = 0;
			
			Bit#(`pfrac) final_man = zeroExtend({1'b1,f1});
      $display("1 %b",  final_man);
			
			final_man = final_man << expo;
      $display("2 %b",  final_man);
			
			if(unsign == 0) begin
			  if(expo < p-1) begin
			    result = final_man[pf-1:fr];
          $display("3 %b", result);
			  end
			  else begin //overflow case
			    Bit#(TSub#(`pwidth,1)) ones = '1;
			    result = {1'b0,ones};
			  end
			end
			else begin //unsign is set
			  flags[1] = 0;
			  if(expo < p) begin
			    result = final_man[pf-1:fr];
			  end
			  else begin //overflow case
			    Bit#(`pwidth) ones = '1;
			    result = ones;
			  end
			end
			
			bit guard = final_man[fr];
			bit round = final_man[fr-1];
      $display("4 %b",round);
			if(rm == 3'b001) round = 0;
			Bit#(TSub#(`frac,1)) man_temp = final_man[fr-2:0];
			bit sticky = |man_temp;
			
			bit sign = (unsign==1)? 0: s1;
			
			if(unsign==1 && s1==1) flags[0] = 1;
			if(expo == -1 && f1 > 0 && rm!= 3'b001) result = 1;

      round = round & (guard | sticky);
		  round = round ^ sign;		
		  if(sign==1) result = ~result;
		  result = result + zeroExtend(round);
		  if(flags[0] == 1) result =0;
		  if(flags[1] == 1) begin result = 0; result[p-1] = 1; end
		  		        
      rg_inputs[0] <= result;
      rg_valid[0] <=  True;
    endmethod
    
    method Bit#(`pwidth) resp;
      return rg_inputs[`ptoi_stages -1];
    endmethod
    
    method Bool resp_valid;
      return rg_valid[`ptoi_stages-1];
    endmethod
    
  endmodule
  
endpackage

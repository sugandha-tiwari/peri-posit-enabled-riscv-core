/*
Author: Sugandha Tiwari
Description: Posit FMA
*/
package fma;

  import unsignedmul :: * ;
  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;  
  import dec :: *;
  import enc :: *;
  import BUtils :: *;
  `include "defined_params.bsv"

  interface Ifc_fma;
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, bit operation, bit neg, Bit#(`es) esval);
    method Bit#(`pwidth) resp;
    method Bool resp_valid;
  endinterface
  
  module mkfma(Ifc_fma);
    
    Vector#(`fma_stages, Reg#(Bit#(`pwidth)))  rg_inputs  <- replicateM(mkReg(unpack(0)));
    Vector#(`fma_stages, Reg#(Bool)) rg_valid  <- replicateM(mkDReg(False));
    Ifc_unsignedmul#(`frac1, `frac1) mult <- mkunsignedmul;
    
    rule rl_start;
      for(Integer i = 1; i <=  `fma_stages-1 ; i = i+ 1) begin
        rg_inputs[i] <= rg_inputs[i - 1];
        rg_valid[i] <= rg_valid[i-1];
      end
      
    endrule
    
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, bit operation, bit
    neg, Bit#(`es) esval);        
        
        let {s1, e1, f1, flags1} = fn_dec(op1, esval);
        let {s2, e2, f2, flags2} = fn_dec(op2, esval);
        let {s3, e3, f3, flags3} = fn_dec(op3, esval);
        
        Bit#(1) prod_sign = s1^s2^neg;
        Int#(`exp1) prod_exp =  signExtend(e1) + signExtend(e2);
        mult.ia({1'b1, f1});
        mult.ib({1'b1, f2});
        Bit#(`frac2) prod_frac = mult.oc;
        
        if(prod_frac[valueof(`frac2)-1] == 1)
          prod_exp = prod_exp + 1;
        else
          prod_frac = prod_frac << 1;
        
        Bit#(1) op3_sign = s3^operation^neg;
        Int#(`exp1) op3_exp =  signExtend(e3);
        Bit#(`frac1) zeros = 0;
        Bit#(`frac2) op3_frac = {1'b1,f3,zeros};
        
        Bit#(1) res_sign = 0;
        Int#(`exp1) res_exp = 0;
        Bit#(`frac3) res_frac = 0;
        Bit#(2) resultFlag = 0;
        Bit#(1) sticky_bit = 0;
        
        Bool op3_dom = False;
        
        Int#(`exp2) exp_diff = signExtend(prod_exp) - signExtend(op3_exp);
        Bit#(1) or_exp_diff = |pack(exp_diff);
        //$display("%d %d", prod_exp,  op3_exp);

        if(pack(exp_diff)[valueof(`exp2)-1]==1) begin
          op3_dom = True;          
        end
        
        else if(or_exp_diff == 0 && op3_frac >prod_frac) begin
          op3_dom = True;
        end
        
        if(or_exp_diff == 0 && prod_frac == op3_frac && prod_sign != op3_sign) begin
          resultFlag[0] = 1;
        end
        
        Bool rset = False;
        bit or_1_2 = flags1[0] | flags2[0];
        
        if((flags1[1] | flags2[1] | flags3[1]) ==1) begin
        	resultFlag[1] = 1;
        end
        
        else if((or_1_2 & flags3[0]) ==1) begin
            resultFlag[0] = 1;
        end
        
      	else if(or_1_2 ==1) begin
          res_sign = op3_sign;
          res_exp = op3_exp;
          res_frac = {1'b0, op3_frac};
          rset = True;
        end
        
        else if(flags3[0] == 1) begin
          res_sign = prod_sign;
          res_exp = prod_exp;
          res_frac = {1'b0, prod_frac};
          rset = True;
				end

        //grt_exp = swap? sml_exp : grt_exp;
        
        //grt_sign = swap? sml_sign : grt_sign;
        //Bit#(frac2) sm_frac = swap? grt_frac: sml_frac;
        //Bit#(frac2) gr_frac = swap? sml_frac: grt_frac;

		exp_diff = op3_dom? abs(exp_diff) : exp_diff;
		bit fma_sign = op3_dom? op3_sign: prod_sign;
		Int#(`exp1) fma_exp = op3_dom? op3_exp: prod_exp;
		//Bit#(frac2) sml_frac = op3_dom? prod_frac: op3_frac;
		
        Int#(`exp2) sticky_shift = fromInteger(valueof(`frac2)) - exp_diff;        
        Bit#(`frac2) sticky_part;
        
        //if(pack(sticky_shift)[valueof(exp2)-1]==0) begin
          //sticky_part = sticky_part << sticky_shift;
        //end
        //sticky_bit = |sticky_part;
        
        if(op3_dom) begin
          sticky_part = prod_frac << sticky_shift;
        	prod_frac = prod_frac >> exp_diff;
       	  //sticky_part = (pack(sticky_shift)[valueof(`exp2)-1]==0)? prod_frac << sticky_shift: prod_frac; // check sticky_shift?
       	end
        else begin
        	sticky_part = op3_frac << sticky_shift;
          op3_frac = op3_frac >> exp_diff;
        	//sticky_part = (pack(sticky_shift)[valueof(`exp2)-1]==0)? op3_frac << sticky_shift: op3_frac; // check sticky_shift?
        end
       	
       	sticky_bit = |sticky_part;

        Bit#(`frac3) pf = zeroExtend(prod_frac);
        Bit#(`frac3) op3f = zeroExtend(op3_frac);
		
		    pf = (duplicate(prod_sign)^pf) +  zeroExtend(prod_sign);
		    op3f = (duplicate(op3_sign)^op3f) +  zeroExtend(op3_sign);
		    Bit#(`frac4) fma_fraction = zeroExtend(pf) + zeroExtend(op3f);
        //Bit#(`frac3) fma_frac = zeroExtend(prod_frac) + zeroExtend(op3_frac);
	    //$display("%d %b",  fma_exp, fma_fraction);	
		    fma_fraction = (duplicate(fma_sign)^fma_fraction) +  zeroExtend(fma_sign);
        //$display("%d %b",  fma_exp, fma_fraction);
        //fma_frac = (duplicate(fma_sign)^fma_frac) +  zeroExtend(fma_sign);
		
		    Bit#(`frac3) fma_frac = truncate(fma_fraction);
      //$display("%d %b",  fma_exp, fma_frac);
          
        //if(or_exp_diff == 0 && (|fma_frac == 0)) begin
          //resultFlag[0] = 1;
        //end        

        if(fma_frac[valueof(`frac3)-1]==1) begin
          sticky_bit = sticky_bit | fma_frac[0];
          fma_frac = fma_frac >> 1;
          fma_exp = fma_exp + 1;
        end

        else if(fma_frac[valueof(`frac3)-2]!=1) begin
          let shift = countZerosMSB(fma_frac);
          shift = shift - 1;
          fma_frac = fma_frac << shift ;
          fma_exp = fma_exp - unpack(zeroExtend(pack(shift)));
        end
        
        if(rset) begin
          fma_sign = res_sign;
          fma_exp = res_exp;
          fma_frac = res_frac;
          sticky_bit = 0;
        end    
		        
        Bit#(TSub#(`frac3,2)) trunc_fma_frac = truncate(fma_frac);
		Bit#(`frac) sticky_bits = truncate(trunc_fma_frac);
		sticky_bit = sticky_bit |(|sticky_bits);
		Bit#(`frac1) new_frac = truncateLSB(trunc_fma_frac);
    //$display("%d %b", fma_exp, new_frac);
		    
        rg_inputs[0] <=  fn_enc(tuple5(resultFlag, fma_sign, fma_exp, new_frac, sticky_bit), esval);
        rg_valid[0] <=  True;
    endmethod
    
    method Bit#(`pwidth) resp;
      return rg_inputs[`fma_stages -1];
    endmethod
    
    method Bool resp_valid;
      return rg_valid[`fma_stages-1];
    endmethod
    
  endmodule
  
endpackage

/*
Author: Sugandha Tiwari
Description: Sign Injection Module
*/

package sign_inject;

  import BUtils :: *;
  `include "defined_params.bsv"
  
  interface Ifc_sign_inject;
    method Bit#(`pwidth) req(Bit#(`pwidth) a, Bit#(1) s_b, Bit#(3) operation);
  endinterface
  
  module mksign_inject(Ifc_sign_inject);
    
    method Bit#(`pwidth) req(Bit#(`pwidth) a, Bit#(1) s_b, Bit#(3) operation);
      let p = valueOf(`pwidth);
      Bit#(1) sign; 
      if(operation == 3'b000) //FSGNJ
	      sign = s_b;
	    else if(operation == 3'b001) //FSNGNJN
	      sign = ~s_b;
	    else //FSGNJX
	      sign = a[p-1]^s_b;
	     
	    a = (duplicate(sign)^a) +  zeroExtend(sign);
      
      return a;
    
    endmethod
    
  endmodule

endpackage

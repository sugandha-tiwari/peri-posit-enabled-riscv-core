/*
Author: Sugandha Tiwari
Description: Posit decoding
*/
package dec;
 
  import BUtils :: *;
  `include "defined_params.bsv"
  
  (*noinline*)
  function Tuple4#(Bit#(1), Int#(`exp), Bit#(`frac), Bit#(2)) fn_dec(Bit#(`pwidth) posit);
    
    let p = fromInteger(valueOf(`pwidth));
    
    Bit#(TSub#(`pwidth, 1)) lower_posit = truncate(posit);
    Bit#(1) sign = posit[p-1];
    Bit#(1) lower_all_zeros = | lower_posit;

    //------------------ flag generation ------------
    Bit#(1) f0 = ~sign & ~lower_all_zeros;
    Bit#(1) fNaR = sign & ~lower_all_zeros;
    Bit#(2) flags= {fNaR, f0};
    // ------------------------------------------------    

    posit = (duplicate(sign)^posit) +  zeroExtend(sign);
		
		lower_posit = truncate(posit);
		Bit#(TSub#(`pwidth,1)) trunc_posit = lower_posit ^ duplicate(lower_posit[p-2]);
		
		Bit#(TLog#(`pwidth)) rc = pack(countZerosMSB(trunc_posit));
		
		Int#(TAdd#(TLog#(`pwidth),1)) count = unpack(zeroExtend(rc));
		Int#(TAdd#(TLog#(`pwidth),1)) k;
		
		if(posit[p-2] == 0) 
			k = -count;
		else 
			k = count - 1;	
	
		lower_posit = lower_posit << rc;
		lower_posit = lower_posit << 1;
		
		Bit#(`es) expo = truncateLSB(lower_posit);

		Int#(`exp) exponent = unpack(zeroExtend(expo)) + (signExtend(k) << `es);	
		
		lower_posit = lower_posit << `es;
		Bit#(`frac) fraction = truncateLSB(lower_posit);
		
		return tuple4(sign,exponent,fraction,flags);
      
  endfunction
endpackage

/*
Author: Sugandha Tiwari
Description: Posit itop
*/
package itop;

  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;
  import BUtils :: *;
  import enc :: *; 
  `include "defined_params.bsv"

  interface Ifc_itop;
    method Action req(Bit#(`pwidth) inp_int, bit unsign);
    method Bit#(`pwidth) resp;
    method Bool resp_valid;
  endinterface
  
  module mkitop(Ifc_itop);
    
    Vector#(`itop_stages, Reg#(Bit#(`pwidth)))  rg_inputs  <- replicateM(mkReg(unpack(0)));
    Vector#(`itop_stages, Reg#(Bool)) rg_valid  <- replicateM(mkDReg(False));
    
    rule rl_start;
      for(Integer i = 1; i <=  `itop_stages-1 ; i = i+ 1) begin
        rg_inputs[i] <= rg_inputs[i - 1];
        rg_valid[i] <= rg_valid[i-1];
      end
      
    endrule
    
    method Action req(Bit#(`pwidth) inp_int, bit unsign);
      
      let p = valueOf(`pwidth);
      Bit#(1) sign = inp_int[p-1];
      Bit#(TSub#(`pwidth,1)) lower_bits = truncate(inp_int);
      Bit#(1) r0 = ~sign & ~|lower_bits;
      
      sign = sign & ~unsign;
      inp_int = (duplicate(sign)^inp_int) +  zeroExtend(sign);
      
      Bit#(TLog#(TAdd#(`pwidth,1))) expo = fromInteger(p)-1;
      
      Bit#(TLog#(TAdd#(`pwidth,1))) zeros = pack(countZerosMSB(inp_int));
      
      inp_int = inp_int << zeros;
      expo = expo - zeros;
      
      Bit#(`frac1) final_frac = inp_int[p-2:p-valueOf(`frac1)-1];
      Bit#(TSub#(`pwidth, TSub#(`frac1,1))) rem_inp = inp_int[p-valueOf(`frac1)-2:0];
      bit sticky = |rem_inp;
      
      Int#(`exp1) exponent = unpack(zeroExtend(expo));
      $display("%b %d %b",  sign,  exponent,  final_frac);
      rg_inputs[0] <= fn_enc(tuple5({1'b0,r0}, sign, exponent, final_frac, sticky));
      rg_valid[0] <=  True;
    endmethod
    
    method Bit#(`pwidth) resp;
      return rg_inputs[`itop_stages -1];
    endmethod
    
    method Bool resp_valid;
      return rg_valid[`itop_stages-1];
    endmethod
    
  endmodule  
endpackage

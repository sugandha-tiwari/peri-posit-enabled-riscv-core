/*
Author: Sugandha Tiwari
Description: Comparision Module
*/

package comparision;

  `include "defined_params.bsv"
  
  interface Ifc_comparision;
    method Bit#(`pwidth) req(Bit#(`pwidth) opr1, Bit#(`pwidth) opr2, Bit#(3) which_cmp_instr, bit cmp_or_min_max);
  endinterface
  
  module mkcomparision(Ifc_comparision);
    
    method Bit#(`pwidth) req(Bit#(`pwidth) opr1, Bit#(`pwidth) opr2, Bit#(3) which_cmp_instr, bit cmp_or_min_max);
    
      Bit#(`pwidth) result = 0;
		  Bit#(2) grt_op = 0;
		  Int#(`pwidth) op1 = unpack(opr1);
		  Int#(`pwidth) op2 = unpack(opr2);
		  if(op1 < op2)
		    grt_op = 2'b10;
		  else if(op1 == op2)
		    grt_op = 2'b11;
		  else
		    grt_op = 2'b01;
		    
		  if(cmp_or_min_max == 0) begin // Compare instruction
		    if((which_cmp_instr == 3'b000 && grt_op != 2'b01 ) || (which_cmp_instr == 3'b001 && grt_op == 2'b10) || (which_cmp_instr == 3'b010 && grt_op == 2'b11)) result[0]=1;
		  end
		  else begin //Min Max instruction
		    if((which_cmp_instr[0]==0 && grt_op == 2'b10) || (which_cmp_instr[0]==1 && grt_op == 2'b01)) result = opr1;//conditions to return operand1
		    else result = opr2;//conditions to return operand2
		  end
		    
		  return result;
    
    endmethod
    
  endmodule

endpackage

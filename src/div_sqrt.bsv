/*
Author: Sugandha Tiwari
Description: Posit Div Sqrt
*/
package div_sqrt;

  import DReg :: *;
  import dec :: *;
  import enc :: *; 
  import bsvmkfrac_div_sqrt :: *;
  `include "defined_params.bsv"

  interface Ifc_div_sqrt;
    method Action req(Bit#(1) inValid, Bit#(1) sqrtOp, Bit#(`pwidth) op1, Bit#(`pwidth) op2);
    method ActionValue#(Tuple2#(Bit#(`pwidth), Bit#(1))) resp;
    method Bool resp_valid;
    method Bit#(2) flags;
  endinterface
  
  module mkdiv_sqrt(Ifc_div_sqrt);
             
    Ifc_frac_div_sqrt#(`frac1) div_sqrt <- mkfrac_div_sqrt;
    
    Reg#(Bit#(1)) out_sign <- mkRegU;
    Reg#(Bit#(1)) out_exc_dz <- mkRegU;
    Reg#(Bit#(1)) out_r0 <- mkRegU;
    Reg#(Bit#(1)) out_rNaR <- mkRegU;
    //Reg#(Bit#(`es)) rg_es <- mkRegU;
    Reg#(Bit#(1)) out_sqrtOp <- mkRegU;
    Reg#(Int#(TAdd#(`exp, 1))) out_exp <- mkRegU;
    
    Reg#(Bit#(1)) rg_inValid <- mkDReg(0);
    Reg#(Bit#(1)) rg_sqrtOp <- mkDReg(0);
    Reg#(Bit#(1)) rg_normalCase <- mkDReg(0);
    Reg#(Bit#(1)) rg_oddSqrt <- mkDReg(0);
    Reg#(Bit#(1)) rg_evenSqrt <- mkDReg(0);
    Reg#(Bit#(TAdd#(`frac1,1))) rg_sig1 <- mkDReg(0);
    Reg#(Bit#(TAdd#(`frac1,1))) rg_sig2 <- mkDReg(0);
    
    rule send_defaults;
      div_sqrt.req(rg_inValid, rg_sqrtOp, rg_normalCase, rg_oddSqrt, rg_evenSqrt, rg_sig1, rg_sig2);
    endrule
    
    method Action req(Bit#(1) inValid, Bit#(1) sqrtOp, Bit#(`pwidth) op1, Bit#(`pwidth) op2);
    
      let {s1, e1, f1, flags1} = fn_dec(op1);
      let {s2, e2, f2, flags2} = fn_dec(op2);
      
      //rg_es <= esval;
      out_sqrtOp <= sqrtOp;
      Bit#(1) divOp = ~sqrtOp;
      out_sign <= s1 ^ (divOp & s2);
      Bit#(1) exc_dz = divOp & flags2[0];
      out_exc_dz <= exc_dz;
      Bit#(1) r0 = flags1[0] & ~exc_dz;
      out_r0 <= r0;
      Bit#(1) rNaR = flags1[1] | exc_dz | (sqrtOp & s1) | (divOp & flags2[1]);
      $display("%b %b",  r0,  rNaR);
      out_rNaR <=  rNaR;
      Bit#(1) normalCase = ~(r0 | rNaR);
      out_exp <= (sqrtOp==1) ? signExtend(e1 >> 1) : signExtend(e1) - signExtend(e2);
      Bit#(1) evenSqrt = sqrtOp & (~pack(e1)[0]);
      Bit#(1) oddSqrt  = sqrtOp & pack(e1)[0];
      
      Bit#(TAdd#(`frac1,1)) sig1 = {2'b01,f1};
      Bit#(TAdd#(`frac1,1)) sig2 = {2'b01,f2};
      
      rg_inValid <= inValid;
      rg_sqrtOp <= sqrtOp;
      rg_normalCase <= normalCase;
      rg_oddSqrt <=  oddSqrt;
      rg_evenSqrt <= evenSqrt;
      rg_sig1 <= sig1;
      rg_sig2 <= sig2;
       
    endmethod
    
    method ActionValue#(Tuple2#(Bit#(`pwidth), Bit#(1))) resp;
      Bit#(TAdd#(`frac1,3)) fract = div_sqrt.oout_sig;
      Int#(`exp1) expo = out_exp;
      Bit#(`frac1) out_frac = fract[valueOf(`frac1)+1:2];
      bit sticky = |fract[1:0];
      
      out_frac = fract[valueOf(`frac1):1];
      sticky = fract[0];      
      
      if(out_sqrtOp==0 && fract[valueOf(`frac1)+2]!=1) begin
        expo = expo - 1;
      end
      else if(out_sqrtOp==0) begin
        out_frac = fract[valueOf(`frac1)+1:2];
        sticky = |fract[1:0];
      end
      //$display("%b %b %d", out_frac, sticky,  expo);
      $display("out_rNaR %b",  out_rNaR);
      let x = fn_enc(tuple5({out_rNaR, out_r0}, out_sign, expo, out_frac, sticky)); 
      return tuple2(x, out_exc_dz);
    endmethod
    
    method Bool resp_valid;
      return div_sqrt.ooutValid;
    endmethod

    method Bit#(2) flags;
      return {out_rNaR,  out_r0};
    endmethod
    
  endmodule
  
endpackage

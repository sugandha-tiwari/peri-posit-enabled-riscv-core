/*
Author: Sugandha Tiwari
Description: Posit encoding
*/
package enc;

  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;  
  
  function Bit#(pwidth) fn_enc(Tuple5#(Bit#(2), Bit#(1), Int#(exp1), Bit#(frac1), Bit#(1)) op1, Bit#(es) esval)
    provisos( Add#(pm1, 1, pwidth),
              Add#(pwidth, 1, p1),
              Add#(exp1mes, es, exp1),
              Add#(exp1mesm1, 1, exp1mes),
              Add#(a__, 2, exp1mesm1)
            );
    
    let p = valueOf(pwidth);
    
    Bit#(2) flag = tpl_1(op1);
    Bit#(1) sign = tpl_2(op1);
    Int#(exp1) grt_exp = tpl_3(op1);
		Bit#(frac1) final_frac = tpl_4(op1);
		Bit#(1) sticky_bit = tpl_5(op1);
		
		Bit#(1) guard_bit = 0;
		Bit#(1) round_bit = 0;
		
		Bit#(es) es_bit = pack(truncate(grt_exp));

		Int#(exp1mes) greater_exponent = unpack(truncateLSB(pack(grt_exp)));
		
		Bit#(2) const_val = 0;
		
		Bit#(p1) ext_result = 1;
		if(pack(greater_exponent)[valueOf(exp1mes)-1] == 0)	begin
			ext_result = '1;
			const_val = 1;
		end
		
		Bit#(exp1mesm1) greater_exp = truncate(pack(abs(greater_exponent)));
		
		Int#(exp1mesm1) regime_shift = fromInteger(valueOf(pm1)) - unpack(greater_exp);
		if(pack(regime_shift)[valueOf(exp1mesm1)-1] == 0) begin
			ext_result = ext_result << regime_shift;
		end
		ext_result[p] = 0;
		Bit#(TSub#(p1,TAdd#(es,frac1))) zero = '0; 
		
		Bit#(p1) es_frac = {es_bit,final_frac,zero};
		
		let expo_shift = greater_exp + 2 +  zeroExtend(const_val);
		Bit#(exp1mesm1) sticky_shift = fromInteger(valueOf(p1)) - expo_shift;
		
		Bit#(p1) sticky_part = es_frac;
		if(sticky_shift[valueOf(exp1mesm1)-1] == 0) begin 
			sticky_part = es_frac << sticky_shift;
			es_frac = es_frac >> expo_shift;
		end
		else begin
			es_frac = 0;
		end
		sticky_bit = sticky_bit |(|sticky_part);
		
		ext_result = ext_result | es_frac ;
		round_bit = ext_result[0];
		guard_bit = ext_result[1];
		
		Bit#(pwidth) result = 0;
		result = truncateLSB(ext_result);
		
		bit round = round_bit & (guard_bit | sticky_bit);
		Bit#(pm1) rtemp = result[valueOf(pm1)-1:0];
		Bit#(1) max_pos = (~result[p-1])&(&rtemp);
		if(max_pos == 1) round = 0;
		bit plus_one = 0;
		plus_one = round ^ sign;		
		if((round|(|result)) == 0) result = 1;
		if(sign==1) result = ~result;
		result = result + zeroExtend(plus_one);
		if(flag[0] == 1) result =0;
		if(flag[1] == 1) begin result = 0; result[valueOf(pm1)] = 1; end
		return result;
		
  endfunction
  
endpackage

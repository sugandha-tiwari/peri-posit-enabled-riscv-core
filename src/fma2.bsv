/*
Author: Sugandha Tiwari
Description: Posit FMA
*/
package fma;

  import unsignedmul :: * ;
  import ConfigReg::*;
  import Vector :: * ;
  import DReg :: * ;  
  import dec :: *;
  import enc :: *; 

  interface Ifc_fma#(numeric type pwidth, numeric type es);
    method Action req(Bit#(pwidth) op1, Bit#(pwidth) op2, Bit#(pwidth) op3, bit operation, bit neg, bit unsign, Bit#(es) esval);
    method Bit#(pwidth) resp;
    method Bool resp_valid;
  endinterface
  
  module mkfma(Ifc_fma#(pwidth, es))
    provisos (Add#(TAdd#(TLog#(pwidth), 1), es, exp),
              Add#(exp, 1, exp1),
              Add#(exp1, 1, exp2),
              Add#(frac, 3, TSub#(pwidth, es)),
              Add#(frac, 1, frac1),
              Add#(frac1, frac1, frac2),
              Add#(frac2, 1, frac3),              
              
              // required for importing dec.bsv
              Add#(a__, frac, TSub#(pwidth, 1)), 
              Add#(frac, a__, TAdd#(es, b__)), 
              Log#(TAdd#(1, TSub#(pwidth, 1)), TLog#(pwidth)),

              // required by bsc
              Add#(c__, frac, TSub#(frac3, 2)), 
              Add#(d__, TLog#(TAdd#(1, frac3)), exp1), 
              Add#(e__, es, exp1), 
              Add#(f__, 1, e__), 
              Add#(g__, 2, f__), 
              Add#(frac1, h__, TAdd#(frac, c__)),
              Add#(frac, c__, TAdd#(frac1, i__))
             );
    
    Vector#(`fma_stages, Reg#(Bit#(pwidth)))  rg_inputs  <- replicateM(mkReg(unpack(0)));
    Vector#(`fma_stages, Reg#(Bool)) rg_valid  <- replicateM(mkDReg(False));
    Ifc_unsignedmul#(frac1, frac1) mult <- mkunsignedmul;
    
    rule rl_start;
      for(Integer i = 1; i <=  `fma_stages-1 ; i = i+ 1) begin
        rg_inputs[i] <= rg_inputs[i - 1];
        rg_valid[i] <= rg_valid[i-1];
      end
      
    endrule
    
    method Action req(Bit#(pwidth) op1, Bit#(pwidth) op2, Bit#(pwidth) op3, bit operation, bit neg, bit unsign, Bit#(es) esval);        
        
        let {s1, e1, f1, flags1} = fn_dec(op1, unsign, esval);
        let {s2, e2, f2, flags2} = fn_dec(op2, unsign, esval);
        let {s3, e3, f3, flags3} = fn_dec(op3, unsign, esval);
        
        //let {s1, e1, f1, flags1} <- dec.req(op1, unsign, esval);
        //let {s2, e2, f2, flags2} <- dec.req(op2, unsign, esval);
        //let {s3, e3, f3, flags3} <- dec.req(op3, unsign, esval);
        
        //$display($time, " s1: %b, e1: %d, f1: %b", s1, e1, f1);
        //$display($time, " s2: %b, e2: %d, f2: %b", s2, e2, f2);
        //$display($time, " s3: %b, e3: %d, f3: %b", s3, e3, f3);
        
        Bit#(1) grt_sign = s1^s2^neg;
        Int#(exp1) grt_exp =  signExtend(e1) + signExtend(e2);
        mult.ia({1'b1, f1});
        mult.ib({1'b1, f2});
        Bit#(frac2) grt_frac = mult.oc;
        
        if(grt_frac[valueof(frac2)-1] == 1)
          grt_exp = grt_exp + 1;
        else
          grt_frac = grt_frac << 1;
        
        Bit#(1) sml_sign = s3^operation^neg;
        Int#(exp1) sml_exp =  signExtend(e3);
        Bit#(frac1) zeros = 0;
        Bit#(frac2) sml_frac = {1'b1,f3,zeros};
        
        Bit#(1) res_sign = 0;
        Int#(exp1) res_exp = 0;
        Bit#(frac3) res_frac = 0;
        Bit#(2) resultFlag = 0;
        Bit#(1) sticky_bit = 0;
        
        Bool swap = False;
        
        Int#(exp2) exp_diff = signExtend(grt_exp) - signExtend(sml_exp);
        Bit#(1) or_exp_diff = |pack(exp_diff);

        if(pack(exp_diff)[valueof(exp2)-1]==1) begin
          swap = True;
        end
        else if(or_exp_diff == 0 && sml_frac > grt_frac) begin
          swap = True;
        end
        else if(or_exp_diff == 0 && sml_frac == grt_frac && sml_sign != grt_sign) begin
          resultFlag[0] = 1;
        end
        
        //let {flags1, flags2, flags3} = flags;
        Bool rset = False;
        bit or_1_2 = flags1[0] | flags2[0];
        
        if((flags1[1] | flags2[1] | flags3[1]) ==1) begin
        	resultFlag[1] = 1;
        end
        
        else if((or_1_2 & flags3[0]) ==1) begin
          resultFlag[0] = 1;
        end
        
      	else if(or_1_2 ==1) begin
          //if(flags3[0]==1) resultFlag[0] = 1;
        	res_sign = sml_sign;
        	res_exp = sml_exp;
        	res_frac = {1'b0, sml_frac};
        	rset = True;
        end
        
        else if(flags3[0] == 1) begin
          res_sign = grt_sign;
        	res_exp = grt_exp;
        	res_frac = {1'b0, grt_frac};
        	rset = True;
		    end

        if(swap) begin
          Bit#(1) temp_sign = grt_sign;
          Int#(exp1) temp_exp = grt_exp;
          Bit#(frac2) temp_frac = grt_frac;
          grt_sign = sml_sign; grt_exp = sml_exp; grt_frac = sml_frac;
          sml_sign = temp_sign; sml_exp = temp_exp; sml_frac = temp_frac;
          exp_diff = abs(exp_diff);
        end     

        Int#(exp2) sticky_shift = fromInteger(valueof(frac2)) - exp_diff;
        Bit#(frac2) sticky_part = sml_frac;
        
        if(pack(sticky_shift)[valueof(exp2)-1]==0) begin
          sticky_part = sml_frac << sticky_shift;
        end
        sticky_bit = |sticky_part;

        sml_frac = sml_frac >> exp_diff;

        Bit#(frac3) fma_frac = 0;
        if(grt_sign == sml_sign)
          fma_frac = zeroExtend(grt_frac) + zeroExtend(sml_frac);
        else
          fma_frac = zeroExtend(grt_frac) - zeroExtend(sml_frac);          

        if(fma_frac[valueof(frac3)-1]==1) begin
          sticky_bit = sticky_bit | fma_frac[0];
          fma_frac = fma_frac >> 1;
          grt_exp = grt_exp + 1;
        end

        else if(fma_frac[valueof(frac3)-2]!=1) begin
          let shift = countZerosMSB(fma_frac);
          shift = shift - 1;
          fma_frac = fma_frac << shift ;
          grt_exp = grt_exp - unpack(zeroExtend(pack(shift)));
        end
        
        if(rset) begin
          grt_sign = res_sign;
          grt_exp = res_exp;
          fma_frac = res_frac;
          sticky_bit = 0;
          //if(swap) begin
          //  grt_sign = sml_sign;
          //  grt_exp = sml_exp;
          //end
        end    
		        
        Bit#(TSub#(frac3,2)) trunc_fma_frac = truncate(fma_frac);
		    Bit#(frac) sticky_bits = truncate(trunc_fma_frac);
		    sticky_bit = sticky_bit |(|sticky_bits);
		    Bit#(frac1) new_frac = truncateLSB(trunc_fma_frac);
		    //$display("resultFlag:%b, div_sign:%b, div_exp:%0d, final_frac:%b ,sticky:%b", resultFlag, grt_sign, grt_exp, new_frac, sticky_bit);
		    
		    //let x <- enc.req(tuple5(resultFlag, grt_sign, grt_exp, new_frac, sticky_bit), esval);
		    //rg_inputs[0] <= x;
        rg_inputs[0] <=  fn_enc(tuple5(resultFlag, grt_sign, grt_exp, new_frac, sticky_bit), esval);
        rg_valid[0] <=  True;
    endmethod
    
    method Bit#(pwidth) resp;
      return rg_inputs[`fma_stages -1];
    endmethod
    
    method Bool resp_valid;
      return rg_valid[`fma_stages-1];
    endmethod
    
  endmodule
  
  (*synthesize*)
  module mkfma_instance(Ifc_fma#(`pwidth,`es));
    let ifc();
    mkfma fma(ifc);
    return (ifc);
  endmodule
  
  module mktb_fma1();
	
	  let fma <- mkfma_instance;	
	  Reg#(int) cycle <- mkReg(0);
	  
	  rule count;
		  cycle<=cycle+1;
	  endrule
	  
	  rule give_input(cycle==10);
		  Bit#(32) op1 = 'h0;
		  Bit#(32) op2 = 'h0;
		  Bit#(32) op3 = 'h80000001;		  
		  $display($time," op1: %h", op1[31:0]);
		  $display($time," op2: %h", op2[31:0]);		  
		  $display($time," op3: %h", op3[31:0]);
		  fma.req(op1, op2, op3, 0, 0, 0, 2'b00);
	  endrule
	  
	  rule get_output(fma.resp_valid);
		  let result = fma.resp;
		  $display($time," Result: %h", result);
		  $finish;
	  endrule
	  
  endmodule
  
  import RegFile::*;
  module mktb_fma(); //bulk mode test

	  let fma <- mkfma_instance;	
	  Reg#(int) count <- mkReg(0);
	  Reg#(Bit#(16)) index <- mkReg(0);
	  Reg#(Bit#(16)) out_index <- mkReg(0);
	  RegFile#(Bit#(16),Bit#(96))  input_data <- mkRegFileFullLoad("random-tests/special_hex");
	  let write_file <- mkReg(InvalidFile);
	  
	  rule open_file(count==0);
		  
		  File wfile <- $fopen( "random-tests/test","w");
		  if ( wfile == InvalidFile ) begin
			  $display("cannot open write file");
			  $finish(0);
		  end
		  write_file <= wfile;
		  count <= 1;
	  endrule
	  
	  rule give_input(count ==1 && index <= 10000);
		  //count<=2;
		  let in = index;
		  Bit#(32) op1 = input_data.sub(in)[95:64];
		  Bit#(32) op2 = input_data.sub(in)[63:32];
		  Bit#(32) op3 = input_data.sub(in)[31:0];
		  Bit#(32) ones = '1;
		  `ifdef verbose $display("op1:%b",op1); `endif
		  `ifdef verbose $display("op2:%b",op2); `endif
		  `ifdef verbose $display("op3:%b",op3); `endif
	  
		  fma.req(op1, op2, op3, 0, 0, 0, 2'b00);
		  index <= index + 1;
	  endrule
	  
	  
	  rule get_output(fma.resp_valid);
		  let {result} = fma.resp();
		  //if(valid) begin
		    //count<=1;
		    $display($time,"\tResult: %h",result);
		    $fwrite(write_file,"%b\n", result);
		    out_index <= out_index + 1;
	    	if(out_index == 216) begin $fclose(write_file);	$finish; end
		  //end
	  endrule	
  endmodule
  
endpackage

`define FP_OPCODE       'b0100
`define FMADD           'b0000
`define FMSUB           'b0001
`define FNMSUB          'b0010
`define FNMADD          'b0011
`define FADD_f5         'b00000
`define FSUB_f5         'b00001
`define FMUL_f5         'b00010
`define FDIV_f5         'b00011
`define FSQRT_f5        'b01011
`define FCMP_f5         'b10100
`define FMMAX_f5        'b00101
`define FCVT_F_I_f5     'b11010
`define FCVT_I_F_f5     'b11000
`define FSGNJN_f5       'b00100
`define FCLASS_f5       'b11100
`define FCVT_S_D_f5     'b01000
`define FMV_X_S_f7      'b1110000
`define FMV_S_X_f7      'b1111000
`define FMV_X_D_f7      'b1110001
`define FMV_D_X_f7      'b1111001


`define exp1mes 7
`define exp1mesm1 6

`define exp 9//8//9
`define exp1 10//9//10
`define exp2 11//10//11

`define frac 26//27//26
`define frac1 27//28//27
`define frac2 54//56//54
`define frac3 55//57//55
`define frac4 56//58//56

`define pm1 31

`define pfrac 58//59//58

`define esfrac 30
`define epwidth 62



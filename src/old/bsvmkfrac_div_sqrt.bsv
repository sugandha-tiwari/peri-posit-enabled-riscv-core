// Bluespec wrapper, created by Import BVI Wizard
// Created on: Tue Jan 21 11:49:04 IST 2020
// Created by: sugandha
// Bluespec version: 2019.05.beta2 2019-05-24 a88bf40db


interface Ifc_frac_div_sqrt#(numeric type sigWidth);
	(*always_enabled*)
	method Bool oinReady ();
	(*always_enabled*)
	method Bool ooutValid ();
	(*always_enabled*)
	method Bit#(TAdd#(sigWidth,3)) oout_sig ();
	(*always_ready , always_enabled*)
	method Action req (Bit#(1) invalid, Bit#(1) sqrtop, Bit#(1) normalcase_s, Bit#(1) oddsqrt_s, Bit#(1) evensqrt_s, Bit#(TAdd#(sigWidth,1)) siga_s, Bit#(TAdd#(sigWidth,1)) sigb_s);
endinterface

import "BVI" frac_div_sqrt =
module mkfrac_div_sqrt  (Ifc_frac_div_sqrt#(sigWidth));

	parameter sigWidth = valueOf(sigWidth);

	default_clock clk_clock;
	default_reset rst_nReset;

	input_clock clk_clock (clock)  <- exposeCurrentClock;
	input_reset rst_nReset (nReset) clocked_by(clk_clock)  <- exposeCurrentReset;


	method inReady oinReady ()
		 clocked_by(clk_clock) reset_by(rst_nReset);
	method outValid ooutValid ()
		 clocked_by(clk_clock) reset_by(rst_nReset);
	method out_sig /* (sigWidth + 2) : 0 */ oout_sig ()
		 clocked_by(clk_clock) reset_by(rst_nReset);
	method req (inValid , sqrtOp , normalCase_S , oddSqrt_S , evenSqrt_S , sigA_S /*sigWidth:0*/, sigB_S /*sigWidth:0*/)
		 enable((*inhigh*)req_enable) clocked_by(clk_clock) reset_by(rst_nReset);

	schedule oinReady CF oinReady;
	schedule oinReady CF ooutValid;
	schedule oinReady CF oout_sig;
	schedule oinReady SB req;
	schedule ooutValid CF ooutValid;
	schedule ooutValid CF oout_sig;
	schedule ooutValid SB req;
	schedule oout_sig CF oout_sig;
	schedule oout_sig SB req;
	schedule req C req;
endmodule



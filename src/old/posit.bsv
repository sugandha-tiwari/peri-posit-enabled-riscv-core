/*
Author: Sugandha Tiwari
Description: Posit Unit top module
*/

package posit;
  import DReg :: * ;
  import fma :: * ;
  import ptoi :: * ;
  import itop :: * ;
  import div_sqrt :: * ;
  import comparision :: * ;
  import classify :: * ;
  import sign_inject :: * ;
  
  `include "defined_params.bsv"

  (*synthesize*)
  module mkfma_instance(Ifc_fma#(`pwidth,`es));
    let ifc();
    mkfma fma(ifc);
    return (ifc);
  endmodule

  (*synthesize*)
  module mkdiv_sqrt_instance(Ifc_div_sqrt#(`pwidth,`es));
    let ifc();
    mkdiv_sqrt div_sqrt(ifc);
    return (ifc);
  endmodule
  
  (*synthesize*)
  module mkitop_instance(Ifc_itop#(`pwidth,`es));
    let ifc();
    mkitop itop(ifc);
    return (ifc);
  endmodule
  
  (*synthesize*)
  module mkptoi_instance(Ifc_ptoi#(`pwidth,`es));
    let ifc();
    mkptoi ptoi(ifc);
    return (ifc);
  endmodule
  
  (*synthesize*)
  module mkcomparision_instance(Ifc_comparision#(`pwidth));
    let ifc();
    mkcomparision cmp(ifc);
    return (ifc);
  endmodule
  
  (*synthesize*)
  module mkclassify_instance(Ifc_classify#(`pwidth));
    let ifc();
    mkclassify classify(ifc);
    return (ifc);
  endmodule
  
  (*synthesize*)
  module mksign_inject_instance(Ifc_sign_inject#(`pwidth));
    let ifc();
    mksign_inject sign_inject(ifc);
    return (ifc);
  endmodule
  
  interface Ifc_posit;
	  method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, Bit#(4) opcode, Bit#(7) f7, Bit#(3) f3, Bit#(2) imm,  Bit#(3) fsr, Bool issp); 
	  method Tuple3#(Bool, Bit#(`pwidth), Bit#(5)) resp;
  endinterface
  
  typedef struct{
    Bit#(`pwidth) op1;
    Bit#(`pwidth) op2;
    Bit#(`pwidth) op3;
    Bit#(4) opcode;
    Bit#(7) f7;
    Bit#(3) f3;
    Bit#(2) imm;
    Bit#(3) fsr;
    Bool    issp;
  }Input_Packet deriving (Bits,Eq);

  (*synthesize*)
  module mkposit(Ifc_posit);

    let fma <- mkfma_instance;
    let div_sqrt <- mkdiv_sqrt_instance;
    let itop <- mkitop_instance;
    let ptoi <- mkptoi_instance;
    let cmp <- mkcomparision_instance;
    let classify <- mkclassify_instance;
    let sign_inject <- mksign_inject_instance;
    
    Reg#(Input_Packet) rg_input <- mkReg(unpack(0));
    Reg#(Bool) rg_valid <- mkDReg(False);
    Reg#(Tuple3#(Bool, Bit#(`pwidth), Bit#(5))) rg_output <- mkDReg(unpack(0));
    Reg#(Bool) rg_in_ready <- mkReg(True);
    
    (*conflict_free="start, output_fma, output_div_sqrt, output_ptoi, output_itop"*)
    
    rule start(rg_valid);
      Bit#(`pwidth) op1  = rg_input.op1;
      Bit#(`pwidth) op2  = rg_input.op2;
      Bit#(`pwidth) op3  = rg_input.op3;
      Bit#(4) opcode     = rg_input.opcode;
      Bit#(7) f7         = rg_input.f7;
      Bit#(3) f3         = rg_input.f3;
      Bit#(2) imm        = rg_input.imm;
      Bit#(3) fsr        = rg_input.fsr;
      Bool issp          = rg_input.issp;
      Bit#(`es) esv = 0;
      
      `ifdef verbose $display($time,"\tposit opcode:%b, funct7:%b, funct3:%b imm:%b",opcode, f7, f3, imm); `endif   
      
      if((f7[6:2]==`FCMP_f5 || f7[6:2] == `FMMAX_f5) && opcode == `FP_OPCODE) begin
        let x = cmp.req(op1, op2, f3, f7[2]);
        `ifdef verbose $display($time,"\tposit Input Comparision: op1: %h, op2: %h, f3: %b, f7: %b",op1, op2, f3, f7[2]); `endif
        rg_output <= tuple3(True, x, 0);
      end
		  if(f7[6:2] ==`FCVT_F_I_f5 && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input ITOP: op1: %h, f3: %b, imm: %b", op1, f3, imm[0]); `endif
		    itop.req(op1, imm[0], esv);
		  end
		  if(f7[6:2] == `FSGNJN_f5 && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input sign injection: op1: %h, op2: %b, f3: %b", op1, op2[`pwidth-1], f3); `endif
		    rg_output <= tuple3(True, sign_inject.req(op1, op2[`pwidth-1], f3), 0);
		  end
		  if(f7[6:2] == `FCVT_I_F_f5 && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input PTOI: op1: %h, f3: %b, imm[0]: %b", op1, f3, imm[0]); `endif
		    ptoi.req(op1, imm[0], f3, esv);
		  end
		  if((f7[6:2] == `FCLASS_f5 && f3=='b001) && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input classify: op1: %h", op1); `endif
		    rg_output <= tuple3(True, classify.req(op1), 0);
		  end
		  if(((f7 == `FMV_X_S_f7 || f7 == `FMV_S_X_f7) && f3 == 'b000) && opcode == `FP_OPCODE) begin
		    rg_output <= tuple3(True, op1, 0);
		  end
		  
      if((f7[6:2] == `FADD_f5 || f7[6:2] == `FSUB_f5) && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input Add/Sub: f7[3:2]: %b, op1: %h, op2: %h, f3: %b",f7[3:2], op1, op2, f3); `endif
		    Bit#(`pwidth) one = 0;
		    one[`pwidth-2] = 1;
		    fma.req(op1, one, op2, f7[2], 0, esv);
		  end
		  else if(f7[6:2] == `FMUL_f5 && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input Mul: op1: %h, op2: %h, f3: %b", op1, op2, f3); `endif
		    fma.req(op1, op2, 0, 0, 0, esv);
		  end
		  else if(opcode == `FMADD || opcode == `FMSUB || opcode == `FNMSUB || opcode == `FNMADD) begin
		    `ifdef verbose $display($time,"\tposit Input FMA: op1: %h, op2: %h, op3: %h f3: %b", op1, op2, op3, f3); `endif
		    fma.req(op1, op2, op3, opcode[0], opcode[1], esv);
		  end

		  if(f7[6:2] == `FDIV_f5 && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input Division: op1: %h, op2: %h, f3: %b", op1, op2, f3); `endif
        div_sqrt.req(1, 0, op1, op2, esv);
        rg_in_ready <= False;
		  end
		  else if(f7[6:2] == `FSQRT_f5 && opcode == `FP_OPCODE) begin
		    `ifdef verbose $display($time,"\tposit Input Sqrt: op1: %h, f3: %b", op1, f3); `endif
		    div_sqrt.req(1, 1, op1, op2, esv);
		    rg_in_ready <= False;
		  end
		  else
		    div_sqrt.req(0, 0, op1, op2, esv);
		  
    endrule
    
    rule output_fma(fma.resp_valid);
      let out = fma.resp;
      `ifdef verbose $display($time,"\tposit Output FMA: out: %h", out); `endif
      rg_output <= tuple3(True, out, 0);
    endrule
    
    rule output_div_sqrt(div_sqrt.resp_valid);
      let {out,f} = div_sqrt.resp;
      `ifdef verbose $display($time,"\tPOSIT: Output Div Sqrt: out: %h, flag: %b", out, f); `endif
      rg_in_ready <= True;
      rg_output <= tuple3(True, out, {1'b0,f,3'b0});
    endrule
    
    rule output_ptoi(ptoi.resp_valid);
      let out = ptoi.resp;
      `ifdef verbose $display($time,"\tposit Output PTOI: out: %h", out); `endif
      rg_output <= tuple3(True, out, 0);
    endrule
   
    rule output_itop(itop.resp_valid);
      let out = itop.resp;
      `ifdef verbose $display($time,"\tposit Output ITOP: out: %h", out); `endif
      rg_output <= tuple3(True, out, 0);
    endrule
    
    method Action req(Bit#(`pwidth) op1, Bit#(`pwidth) op2, Bit#(`pwidth) op3, Bit#(4) opcode, Bit#(7) f7, Bit#(3) f3, Bit#(2) imm,  Bit#(3) fsr, Bool issp) if (rg_in_ready && !rg_valid);
      rg_valid <= True;
      rg_input <= Input_Packet {
                    op1 : op1,
                    op2 : op2,
                    op3 : op3,
                    opcode   : opcode,
                    f7   : f7,
                    f3   : f3,
                    imm      : imm,
                    fsr      : fsr,
                    issp     : issp
                  };
    endmethod
    
	  method Tuple3#(Bool, Bit#(`pwidth), Bit#(5)) resp;
	    return rg_output;
	  endmethod
	  
  endmodule
  
endpackage

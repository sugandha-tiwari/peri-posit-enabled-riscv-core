/*
Author: Sugandha Tiwari
Description: Classify Module
*/

package classify;
  
  interface Ifc_classify#(numeric type pwidth);
    method Bit#(pwidth) req(Bit#(pwidth) posit);
  endinterface
  
  module mkclassify(Ifc_classify#(pwidth));
    
    method Bit#(pwidth) req(Bit#(pwidth) posit);
    
      let p = valueOf(pwidth);
      Bit#(TSub#(pwidth, 1)) lower_posit = truncate(posit);
      Bit#(1) sign = posit[p-1];
      Bit#(1) lower_all_zeros = | lower_posit;
      
      Bit#(1) f0 = ~sign & ~lower_all_zeros;
      Bit#(1) fNaR = sign & ~lower_all_zeros;
      
      Bit#(pwidth) result = 0;
		  if(f0 == 1) result[0] = 1; // 0
      else if(fNaR == 1) result[1] = 1; // NaR
      else if(sign == 1) result[2] = 1; // negative
      else if(sign == 0) result[3] = 1; // positive
		  return result;
    endmethod
    
  endmodule

endpackage

package tb_posit;

import RegFile::*;
import posit::*;
`define verbose

module mktb_posit1();
	
	Ifc_posit posit <- mkposit();	
	Reg#(int) cycle <- mkReg(0);
	
	rule count;
		cycle<=cycle+1;
	endrule
	
	rule give_input(cycle==10);
		Bit#(32) op1 = 'h00000000;
    Bit#(32) op2 = 'h00000000;
		Bit#(32) op3 = 'h3b4aab5e;
		$display($time,"\tcycle: %0d op1:%h",cycle,op1[31:0]);
		$display($time,"\tcycle: %0d op2:%h",cycle,op2[31:0]);
		$display($time,"\tcycle: %0d op3:%h",cycle,op3[31:0]);
		
		//posit.req(op1,op2,op3,4'b0100,7'b0000000,0,0,0,True); //Add
		//posit.req(op1,op2,op3,4'b0100,7'b0000100,0,0,0,True); //Sub
		//posit.req(op1,op2,op3,4'b0100,7'b0001000,0,0,0,True); //Mul
		//posit.req(op1,op2,op3,4'b0000,7'b0,0,0,0,True); //fma	
		//posit.req(op1,op2,op3,4'b0001,7'b0000000,0,0,0,True); //fmsub
		//posit.req(op1,op2,op3,4'b0010,7'b0000000,0,0,0,True); //fnmsub
		//posit.req(op1,op2,op3,4'b0011,7'b0000000,0,0,0,True); //fnmadd
		posit.req(op1,op2,op3,4'b0100,7'b0001100,0,0,0,True); //Div
		//posit.req(op1,op2,op3,4'b0100,7'b0101100,0,0,0,True); //Sqrt		
		//posit.req(op1,op2,op3,4'b0100,7'b1010000,2,0,0,True); //FCMP_f5 (FEQ.S, FLT.S, FLE.S)
		//posit.req(op1,op2,op3,4'b0100,7'b0010100,0,0,0,True); //FMMAX_f5 (FMIN.S, FMAX.S)
		//posit.req(op1,op2,op3,4'b0100,7'b1101000,0,2'b00,0,True); //FCVT_F_I_f5 int to float
    //posit.req(op1,op2,op3,4'b0100,7'b1101000,0,2'b01,0,True); //FCVT_F_I_f5 int to float		
		//posit.req(op1,op2,op3,4'b0100,7'b0010000,2,0,0,True); //FSGNJN_f5
		//posit.req(op1,op2,op3,4'b0100,7'b1100000,0,2'b00,0,True); //FCVT_I_F_f5 float to int
		//posit.req(op1,op2,op3,4'b0100,7'b0100000,0,0,0,True); //FCVT_S_D_f5 single to double
		//posit.req(op1,op2,op3,4'b0100,7'b1110000,0,0,0,True); //FMV_X_S_f7
		//posit.req(op1,op2,op3,4'b0100,7'b1111000,0,0,0,True); //FMV_S_X_f7
		//posit.req(op1,op2,op3,4'b0100,7'b1110001,0,0,0,True); //FMV_X_D_f7
		//posit.req(op1,op2,op3,4'b0100,7'b1111001,0,0,0,True); //FMV_D_X_f7
		
	endrule
	
	rule get_output(cycle>10);
		let {valid, result, flags} = posit.resp();
		if(valid) begin
		  $display($time,"\tcycle: %0d Result: %h Flags: %b",cycle,result, flags);
		  $finish;
		end
	endrule
	
endmodule

module mktb_posit(); //bulk mode test

  Ifc_posit posit <- mkposit();
	Reg#(int) count <- mkReg(0);
	Reg#(Bit#(16)) tst_index <- mkReg(215);
	Reg#(Bit#(16)) index <- mkReg(0);
	Reg#(Bit#(16)) out_index <- mkReg(0);
	RegFile#(Bit#(16),Bit#(96))  input_data <- mkRegFileFullLoad("random-tests/special_hex");
	let write_file <- mkReg(InvalidFile);
	
	rule open_file(count==0);
		
		File wfile <- $fopen( "random-tests/test","w");
		if ( wfile == InvalidFile ) begin
			$display("cannot open write file");
			$finish(0);
		end
		write_file <= wfile;
		count <= 1;
	endrule
	
	rule give_input(count == 1 && index <= tst_index);
		let in = index;
		Bit#(32) op1 = input_data.sub(in)[95:64];
		Bit#(32) op2 = input_data.sub(in)[63:32];
		Bit#(32) op3 = input_data.sub(in)[31:0];
		`ifdef verbose $display($time,"\top1:%h",op1); `endif
		`ifdef verbose $display($time,"\top2:%h",op2); `endif
		`ifdef verbose $display($time,"\top3:%h",op3); `endif
	
		//posit.req(op1,op2,op3,4'b0100,7'b0000000,0,0,0,True); //Add
		//posit.req(op1,op2,op3,4'b0100,7'b0000100,0,0,0,True); //Sub
		//posit.req(op1,op2,op3,4'b0100,7'b0001000,0,0,0,True); //Mul
		//posit.req(op1,op2,op3,4'b0000,7'b0001000,0,0,0,True); //fma		
		//posit.req(op1,op2,op3,4'b0001,7'b0000000,0,0,0,True); //fmsub
		//posit.req(op1,op2,op3,4'b0010,7'b0000000,0,0,0,True); //fnmsub
		//posit.req(op1,op2,op3,4'b0011,7'b0000000,0,0,0,True); //fnmadd
		//posit.req(op1,op2,op3,4'b0100,7'b0001100,0,0,0,True); //Div
		//posit.req(op1,op2,op3,4'b0100,7'b0101100,0,0,0,True); //Sqrt
		//posit.req(op1,op2,op3,4'b0100,7'b1010000,0,0,0,True); //FCMP_f5 (FLE.S, FLT.S, FEQ.S)
		//posit.req(op1,op2,op3,4'b0100,7'b1101000,0,2'b00,0,True); //FCVT_F_I_f5 int to float
    posit.req(op1,op2,op3,4'b0100,7'b1101000,0,2'b01,0,True); //FCVT_F_I_f5 uint to float		
		//posit.req(op1,op2,op3,4'b0100,7'b1100000,0,2'b00,0,True); //FCVT_I_F_f5 float to int
		//posit.req(op1,op2,op3,4'b0100,7'b1100000,0,2'b01,0,True); //FCVT_I_F_f5 float to uint
		index <= index + 1;
	endrule
	
	
	rule get_output;
		let {valid, result, flags} = posit.resp();
		if(posit.valid) begin
		  $display($time,"\tResult: %h Flags: %b",result, flags);
		  $fwrite(write_file,"%b\n", result);
		  out_index <= out_index + 1;
	  	if(out_index == tst_index) begin $fclose(write_file);	$finish; end
		end
	endrule
	
endmodule

endpackage

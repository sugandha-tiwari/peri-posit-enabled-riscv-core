// Bluespec wrapper, created by Import BVI Wizard
// Created on: Fri Dec 27 18:28:43 IST 2019
// Created by: sugandha
// Bluespec version: 2019.05.beta2 2019-05-24 a88bf40db


interface Ifc_bsvfma;
	(*always_enabled*)
	method Bit#(32) resp();
	//(*always_enabled*)
	//method Bit#(5) oexceptionFlags ();
	(*always_ready , always_enabled*)
	method Action req (Bit#(32) io_num1, Bit#(32) io_num2, Bit#(32) io_num3, bit io_sub, bit io_negate);
endinterface

import "BVI" PositFMA =
module mkbsvfma  (Ifc_bsvfma);

	default_clock clk_clk;
	default_reset rst;
	
	//parameter expWidth = valueOf(expWidth);
  //parameter sigWidth = valueOf(sigWidth);

	input_clock clk_clk (clk)  <- exposeCurrentClock;
	input_reset rst (/* empty */) clocked_by(clk_clk)  <- exposeCurrentReset;


	method io_out /* (expWidth + sigWidth-1) : 0 */ resp ()
	 clocked_by(clk_clk) reset_by(rst);
	//method exceptionFlags /* 4 : 0 */ oexceptionFlags ()
	//	 clocked_by(clk_ck) reset_by(rst);
	method req (io_num1, io_num2, io_num3, io_sub, io_negate) enable((*inhigh*)request_enable) clocked_by(clk_clk) reset_by(rst);

//	schedule oout CF oout;
//	schedule oout CF oexceptionFlags;
//	schedule oout SB request;
//	schedule oexceptionFlags CF oexceptionFlags;
//	schedule oexceptionFlags SB request;
//	schedule request C request;
endmodule


